﻿using Amazon.Lambda.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Notification.Core.interfaces
{
    public interface IEventHandler<TInput>
    {
        Task HandleAsync(TInput input, ILambdaContext context);
    }

}
