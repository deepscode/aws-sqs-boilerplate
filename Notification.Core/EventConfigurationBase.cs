﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Notification.Core
{
    public class EventConfigurationBase
    {
        protected EventConfigurationBase()
        {
            var services = new ServiceCollection();

            var builder = new ConfigurationBuilder();

            Configure(builder);

            Configuration = builder.Build();


            var EnvironmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production";

            services.AddSingleton(Configuration);

            services.AddLogging(logging => ConfigureLogging(logging, EnvironmentName));

            ConfigureServices(services, EnvironmentName);

            ServiceProvider = services.BuildServiceProvider();

            Logger = ServiceProvider.GetRequiredService<ILogger<EventConfigurationBase>>();

        }

        protected virtual void Configure(IConfigurationBuilder builder) { }

        protected virtual void ConfigureServices(IServiceCollection services, string executionEnvironment) { }

        protected virtual void ConfigureLogging(ILoggingBuilder logging, string Environment) { }

        protected IConfigurationRoot Configuration { get; }

        protected IServiceProvider ServiceProvider { get; }

        protected ILogger Logger { get; }
    }
}
