﻿using Amazon.Lambda.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Notification.Core.interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Notification.Core
{
    public class EventConfiguration <TInput> : EventConfigurationBase
    {
        public async Task FunctionHandlerAsync(TInput input, ILambdaContext context)
        {
            using (var scope = ServiceProvider.CreateScope())
            {
                var handler = scope.ServiceProvider.GetService<IEventHandler<TInput>>();

                if (handler == null)
                {
                    Logger.LogCritical($"No IEventHandler<{typeof(TInput).Name}> could be found.");
                    throw new InvalidOperationException($"No IEventHandler<{typeof(TInput).Name}> could be found.");
                }

                Logger.LogInformation("Invoking handler");
                await handler.HandleAsync(input, context).ConfigureAwait(false);
            }
        }

        protected void RegisterHandler<THandler>(IServiceCollection services) where THandler : class, IEventHandler<TInput>
        {
            services.AddTransient<IEventHandler<TInput>, THandler>();
        }
    }
   

}
