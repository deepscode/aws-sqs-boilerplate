﻿using Amazon.Lambda.Core;
using boilerplateSQS.interfaces;
using boilerplateSQS.modal;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace boilerplateSQS.handlers
{
    public class MessageHandler : IMessageHandler<Message>
    {

        private readonly ILogger<MessageHandler> _logger;

        public MessageHandler(ILogger<MessageHandler> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public Task<string> HandleAsync(Message message, ILambdaContext context)
        {
            _logger.LogInformation($"Received notification: {message.Title}");

            return Task.FromResult(message.Title.ToUpper());
        }
    }
}
