using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Notification.Core;
using boilerplateSQS.Extension;
using boilerplateSQS.modal;
using boilerplateSQS.handlers;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace boilerplateSQS
{
    public class Function : EventConfiguration<SQSEvent>
    {
        protected override void Configure(IConfigurationBuilder builder)
        {
            // Use this method to register your configuration flow. Exactly like in ASP.NET Core
        }

        protected override void ConfigureLogging(ILoggingBuilder logging, string executionEnvironment)
        {
            // Use this method to install logger providers
        }

        protected override void ConfigureServices(IServiceCollection services, string executionEnvironment)
        {
            // You need this line to register your handler
            services.UseSqsHandler<Message, MessageHandler>();

            // Use this method to register your services. Exactly like in ASP.NET Core
        }
    }
}
