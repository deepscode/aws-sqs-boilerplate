﻿using Amazon.Lambda.SQSEvents;
using boilerplateSQS.handlers;
using boilerplateSQS.interfaces;
using Microsoft.Extensions.DependencyInjection;
using Notification.Core.interfaces;


namespace boilerplateSQS.Extension
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection UseSqsHandler<TMessage, THandler>(this IServiceCollection services)
            where TMessage : class
            where THandler : class, IMessageHandler<TMessage>
        {
            services.AddTransient<IEventHandler<SQSEvent>, SqsEventHandler<TMessage>>();

            services.AddTransient<IMessageHandler<TMessage>, THandler>();

            return services;
        }
    }
}
