﻿using Amazon.Lambda.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace boilerplateSQS.interfaces
{
    public interface IMessageHandler<in TMessage> where TMessage : class
    {
        Task<string> HandleAsync(TMessage message, ILambdaContext context);
    }
}
