﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace boilerplateSQS.modal
{
    public class Message
    {
        [JsonProperty("title")]
        public string Title { get; set; }
    }
}
