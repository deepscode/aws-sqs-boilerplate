﻿using Amazon.Lambda.Core;
using Amazon.Lambda.SQSEvents;
using Amazon.Lambda.TestUtilities;
using boilerplateSQS.handlers;
using boilerplateSQS.interfaces;
using boilerplateSQS.modal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace NotificationService.Test
{
    
    public class SQSEventHandlerTest
    {

        private Mock<IMessageHandler<Message>> mockMessageHandler;
        private Mock<IServiceScopeFactory> mockServiceScopeFactory;
        private Mock<IServiceProvider> mockServiceProvider;
        private Mock<ILoggerFactory> mockLoggerFactory;


        
        public SQSEventHandlerTest()
        {
            mockMessageHandler = new Mock<IMessageHandler<Message>>();
            mockMessageHandler.Setup(p => p.HandleAsync(It.IsAny<Message>(), It.IsAny<ILambdaContext>())).Returns(Task.FromResult("Test Completed"));

            mockServiceScopeFactory = new Mock<IServiceScopeFactory>();
            mockServiceScopeFactory.Setup(p => p.CreateScope()).Returns(Mock.Of<IServiceScope>());

            mockServiceProvider = new Mock<IServiceProvider>();
            mockServiceProvider.Setup(p => p.GetService(typeof(IMessageHandler<Message>)))
                               .Returns(mockMessageHandler.Object);
            mockServiceProvider.Setup(p => p.GetService(typeof(IServiceScopeFactory)))
                               .Returns(mockServiceScopeFactory.Object);

            mockLoggerFactory = new Mock<ILoggerFactory>();
            mockLoggerFactory.Setup(p => p.CreateLogger(It.IsAny<string>()))
                             .Returns(Mock.Of<ILogger>());
        }

        private SqsEventHandler<Message> CreateSystemUnderTest()
        {
            return new SqsEventHandler<Message>(mockServiceProvider.Object, mockLoggerFactory.Object);
        }

        [Fact]
        public async Task HandleAsync_resolves_MessageHandler_for_each_record()
        {
            var sqsEvent = new SQSEvent
            {
                Records = new List<SQSEvent.SQSMessage>
                {
                    new SQSEvent.SQSMessage
                    {
                        Body = "{}"
                    },
                    new SQSEvent.SQSMessage
                    {
                        Body = "{}"
                    },
                }
            };

            var lambdaContext = new TestLambdaContext();

            var sut = CreateSystemUnderTest();

            await sut.HandleAsync(sqsEvent, lambdaContext);

            mockServiceProvider.Verify(p => p.GetService(typeof(IMessageHandler<Message>)), Times.Exactly(sqsEvent.Records.Count));
        }

        [Fact]
        public async Task HandleAsync_creates_a_scope_for_each_record()
        {
            var sqsEvent = new SQSEvent
            {
                Records = new List<SQSEvent.SQSMessage>
                {
                    new SQSEvent.SQSMessage
                    {
                        Body = "{}"
                    },
                    new SQSEvent.SQSMessage
                    {
                        Body = "{}"
                    },
                }
            };

            var lambdaContext = new TestLambdaContext();

            var sut = CreateSystemUnderTest();

            await sut.HandleAsync(sqsEvent, lambdaContext);

            mockServiceScopeFactory.Verify(p => p.CreateScope(), Times.Exactly(sqsEvent.Records.Count));
        }

        [Fact]
        public async Task HandleAsync_executes_NotificationHandler_for_each_record()
        {
            var sqsEvent = new SQSEvent
            {
                Records = new List<SQSEvent.SQSMessage>
                {
                    new SQSEvent.SQSMessage
                    {
                        Body = "{}"
                    },
                    new SQSEvent.SQSMessage
                    {
                        Body = "{}"
                    },
                }
            };

            var lambdaContext = new TestLambdaContext();

            var sut = CreateSystemUnderTest();

            await sut.HandleAsync(sqsEvent, lambdaContext);

            mockMessageHandler.Verify(p => p.HandleAsync(It.IsAny<Message>(), lambdaContext), Times.Exactly(sqsEvent.Records.Count));
        }

        [Fact]
        public void HandleAsync_throws_InvalidOperation_if_NotificationHandler_is_not_registered()
        {
            var sqsEvent = new SQSEvent
            {
                Records = new List<SQSEvent.SQSMessage>
                {
                    new SQSEvent.SQSMessage
                    {
                        Body = "{}"
                    },
                    new SQSEvent.SQSMessage
                    {
                        Body = "{}"
                    },
                }
            };

            var lambdaContext = new TestLambdaContext();

            mockServiceProvider = new Mock<IServiceProvider>();
            mockServiceProvider.Setup(p => p.GetService(typeof(IServiceScopeFactory))).Returns(mockServiceScopeFactory.Object);

            var sut = CreateSystemUnderTest();

            Assert.ThrowsAsync<InvalidOperationException>(() => sut.HandleAsync(sqsEvent, lambdaContext));
        }
    }
}
